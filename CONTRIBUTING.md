Contributions are welcome.

 * Please create an issue/ticket before making any large changes.
 * If unit tests are practical for your change, consider adding them.
 * Testing primarily is done though running the example files with real hardware. Please add new examples if relevant.
