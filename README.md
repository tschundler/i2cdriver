# Excamera I2CDriver [![crates.io page](https://img.shields.io/crates/v/i2cdriver)](https://crates.io/crates/i2cdriver) [![docs.rs](https://docs.rs/i2cdriver/badge.svg)](https://docs.rs/i2cdriver)

This is a support library / driver for using the Excamera
[I2CDriver](https://i2cdriver.com/) or
[I2CMini](https://i2cdriver.com/mini.html) devices within Rust/Crablang. It is
intended to facilitate the development of
[embedded-hal](https://crates.io/crates/embedded-hal)-compatible drivers for I2C
devices.

This project is not associated with [Excamera](https://excamera.com/) / James
Bowman.

See [CONTRIBUTING](CONTRIBUTING.md) if you are interested in submitting patches.

## Basic Usage

The I2CDriver can be initialized with the name of a serial port.

```ignore
let mut i2c = I2CDriver::open("/dev/ttyUSB0").unwrap();

let mut d = my_i2c_driver::Driver::new(i2c).unwrap();
d.do_the_thing()
```

Depending on your OS, the port name may appear differently.

- Linux: `/dev/ttyUSB0` or `/dev/ttyS0`; see the output of `sudo dmesg` for the
  exact device name.
  - To avoid buffering delays, run `setserial /dev/ttyUSB0 low_latency`
- MacOS: `/dev/ttyACM0`; see the output of `sudo dmesg` for the exact device
  name.
- Windows: `COM5`; see Device Manager for the exact name.
  - To avoid buffering delays, in Device Manager as Administrator, set the Latency to 1ms in "Advanced" under "Port Settings" - https://www.instructables.com/Lampduino-an-8x8-RGB-Floor-Lamp/#step20

## HAL Support

This implements both 0.2.x and 1.x embedded-hal traits. They are both enabled by
default, but may be controlled by feature flags, `eh0` and `eh1`.

There is embedded-hal-async support using the `async` feature. It is not enabled
by default as it requires nightly compiler to build.

## Testing Using Examples

One convenient way to manually test changes against real hardware is using
[`cargo watch`](https://crates.io/crates/cargo-watch) combined with example
files.

```ignore
$ git clone git@gitlab.com:tschundler/i2cdriver.git
cd i2cdriver
cargo watch -- cargo run --example scanbus -- -d /dev/ttyUSB0
```
