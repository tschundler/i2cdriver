use anyhow::{bail, Context, Result};
use i2cdriver::I2CDriver;
use lis3dh::{
    accelerometer::{RawAccelerometer, Tracker},
    Lis3dh, SlaveAddr,
};

fn main() -> Result<()> {
    let flags = xflags::parse_or_exit! {
        /// Serial port to use.
        optional -d, --device device: String
    };

    let mut i2c = I2CDriver::open(flags.device.unwrap_or("/dev/ttyUSB0".to_string()).as_str())
        .context("open I2CDriver")?;

    i2c.reset()?;

    // There is a default and also alternate default for this device.
    // Doing a bus scan allows using whichever is discovered.
    let Some(addr) = i2c
        .scan()?
        .into_iter()
        .filter_map(|a| match a {
            x if x == SlaveAddr::Default as u8 => Some(SlaveAddr::Default),
            x if x == SlaveAddr::Alternate as u8 => Some(SlaveAddr::Alternate),
            _ => None,
        })
        .next()
    else {
        bail!("device not found in bus scan")
    };

    println!("found at 0x{:x} ({})", addr.addr(), addr.addr());

    let mut lis3dh = Lis3dh::new_i2c(i2c, addr).unwrap();
    lis3dh.set_range(lis3dh::Range::G8).unwrap();

    let mut tracker = Tracker::new(3700.0);

    loop {
        let accel = lis3dh.accel_raw().unwrap();
        let orientation = tracker.update(accel);
        println!("{:?}", orientation);
        std::thread::sleep(std::time::Duration::new(1, 0))
    }
}
