use anyhow::{Context, Result};
use i2cdriver::I2CDriver;
//use xflags;

fn main() -> Result<()> {
    let flags = xflags::parse_or_exit! {
        /// Serial port to use.
        optional -d, --device device: String
    };

    let mut i2c = I2CDriver::open(flags.device.unwrap_or("/dev/ttyUSB0".to_string()).as_str())
        .context("open I2CDriver")?;

    println!("open");

    // Echo one character tp confirm basic communication.
    let got = i2c.echo(b'a')?;
    if got != b'a' {
        panic!("echo test expected 'a', got {}", got);
    }

    // Reset the device. (this is not normally necessary, just a test.)
    i2c.reboot()?;
    std::thread::sleep(std::time::Duration::new(1, 0));

    // Reset bus.
    i2c.reset()?;

    // Collect device status.
    let status = i2c.status()?;
    println!("status:\n\t{:?}\n", status);

    // Scan the bus for devices.
    let found = i2c.scan()?;
    println!(
        "found devices:\n\t{}\n",
        found
            .into_iter()
            .map(|x| format!("{} (0x{:02x})", x, x))
            .collect::<Vec<String>>()
            .join(",  ")
    );

    // Monitor mode changes the mode of the I2CDriver so it must be reassigned.
    println!("monitoring for 1 second...");
    let i2c = i2c.start_monitor()?;
    // An "M" will display in the top-left corner of the LCD when monitoring.
    std::thread::sleep(std::time::Duration::new(1, 0));
    i2c.stop_monitor()?;
    println!("done");

    Ok(())
}
