use std::str::FromStr;

use crate::error::Error;

/// I2C Driver status report.
#[derive(Debug)]
pub struct Status {
    pub name: String,
    pub serial: String,
    pub uptime: u64,
    pub volts: f32,
    pub amps: f32,
    pub temp_c: f32,
    pub mode: String,
    pub sda: u8,
    pub scl: u8,
    pub speed_khz: u16,
    pub pullup: u8,
    pub crc: u16,
}

impl FromStr for Status {
    type Err = Error;

    /// Parses I2C Driver status report, eg `[i2cdriver1 DB000000 000000065 5.183 000 31.4 I 1 1 100 24 ffff                ]`
    ///
    /// _Intended for use with [`str::parse()`]_
   fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (name, serial, uptime, volts, current, temp, mode, sda, scl, speed_khz, pullup, crc) =
            scan_fmt!(s, "[{} {} {d} {f} {f} {f} {} {d} {d} {d} {d} {x}]",
                String, String, u64, f32, f32, f32, String, u8, u8, u16, u8, [hex u16])
            .map_err(|e| Error::Protocol(e.into()))?;
        Ok(Status {
            name,
            serial,
            uptime,
            volts,
            amps: current * 0.001,
            temp_c: temp,
            mode,
            sda,
            scl,
            speed_khz,
            pullup,
            crc,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_str() {
        let s = "[i2cdriver1 DB000000 00065 5.183 12 31.4 I 1 1 100 24 ffff   ]";
        let status = Status::from_str(s).unwrap();

        assert_eq!(status.name, "i2cdriver1");
        assert_eq!(status.serial, "DB000000");
        assert_eq!(status.uptime, 65);
        assert_eq!(status.volts, 5.183);
        assert_eq!(status.amps, 0.012);
        assert_eq!(status.temp_c, 31.4);
        assert_eq!(status.mode, "I");
        assert_eq!(status.sda, 1);
        assert_eq!(status.scl, 1);
        assert_eq!(status.speed_khz, 100);
        assert_eq!(status.pullup, 24);
        assert_eq!(status.crc, 0xffff);
    }
}